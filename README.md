* A simple example of webpack+babel+bulma+umbrellajs+sass-loader+ejs-loader

Usage
- `$ npm install` to resolve dependencies
- `$ npm run build` to compile
- `$ npm run build:verbose` to compile with detailed error reports
- `$ npm start` to exec webpack-dev-server
