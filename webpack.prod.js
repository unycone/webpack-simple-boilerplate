const merge = require('webpack-merge');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const common = require('./webpack.common.js');
const webpack = require('webpack')

module.exports = merge(common, {
  plugins: [
    new webpack.optimize.AggressiveMergingPlugin(),
    new MinifyPlugin(),
  ]
})
