const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractPlugin = new ExtractTextPlugin({
   filename: 'main.css'
})
const GoogleFontsPlugin = require("google-fonts-webpack-plugin")

module.exports = {
  entry: 'entry.js',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './public_html/'),
  },

  module: {
    rules: [
      // compile js
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
		use: {
		  loader: 'babel-loader',
		  options: {
			presets: [
              ["env", {
                // check targets here:
                // http://browserl.ist/?q=last+2+Chrome+versions%2C+last+2+ChromeAndroid+versions%2C+last+2+iOS+versions%2C+last+2+Safari+versions%2C+last+2+Firefox+versions
                "targets": { "browsers": [
                  'last 2 Chrome versions',
                  'last 2 ChromeAndroid versions',
                  'last 2 iOS versions',
                  'last 2 Safari versions',
                  'last 2 Firefox versions'
                ]},
                "useBuiltIns": "usage",
                "modules": false
              }],
            ],
		  }
        },
      },
      // compile css
	  {
		test: /\.(css|scss|sass)$/,
		use: extractPlugin.extract({
		  use: ['css-loader', 'sass-loader']
		})
      },
      // compile ejs to html
      {
        test: /\.ejs$/,
        use: 'ejs-compiled-loader'
      },
      // handle images
      {
        test: /\.(png|jp(e*)g|svg)$/,  
        use: [{
          loader: 'url-loader',
          options: { 
            limit: 8000, // Convert images < 8kb to base64 strings
            name: '[name].[ext]'
          } 
        }]
      },
    ],
  },

  resolve: {
    modules: [
      "node_modules",
      path.resolve(__dirname, 'src'),
    ],
    extensions: [
      '.js',
    ],
  },

  plugins: [
    extractPlugin,
    new HtmlWebpackPlugin({
      template: 'src/views/index.ejs',
      filename: 'index.html',
    }),
    new GoogleFontsPlugin({
      fonts: [
        { family: 'Lato' },
      ]
    }),
  ],

  devtool: 'cheap-source-map',

  devServer: {
    publicPath: "/",
    contentBase: "./public_html",
    host: "0.0.0.0"
  },  
}
