const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  devtool: 'cheap-source-map',
  devServer: {
    publicPath: "/",
    contentBase: "./public_html",
    watchContentBase: true,
  },  
})
