const fontawesome = require('@fortawesome/fontawesome')

const icons = [
  require('@fortawesome/fontawesome-free-solid/faThumbsUp'),
]

fontawesome.library.add(...icons)
